#!/bin/bash

# Install my own lfsbuild dev depenencies
appin python-pip pandoc inotify-tools \
git-core curl build-essential openssl libssl-dev \
rubygems php5 php-pear lua5.2 luarocks sshfs

# Install Python packages
sudo pip install grip markdown

# Install Ruby packages
sudo gem install rake


# Install node
git clone https://github.com/joyent/node.git
cd node
git checkout v0.11.14
./configure
make -j 6
sudo make install

# install npm
curl https://npmjs.org/install.sh | sudo sh

# install global node/npm packages
sudo npm install -g generator-radic bower grunt-cli lodash-cli