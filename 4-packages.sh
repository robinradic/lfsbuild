#!/bin/bash
mkdir -v $LFS/sources
chmod -v a+wt $LFS/sources

# download all of the packages and patches
wget http://www.linuxfromscratch.org/lfs/view/stable/wget-list
wget -i wget-list -P $LFS/sources
pushd $LFS/sources
md5sum -c md5sums
popd

mkdir -v $LFS/tools
ln -sv $LFS/tools /