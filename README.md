#### start

```bash
cd /lfsbuild

./0-version-check.sh
./0-library-check.sh

./1-install-req.sh
./1-install-dev.sh

# on empty virtual disk make partition
fdisk /dev/sdb
# press "o" to create partition table
# press "w" to write

cfdisk /dev/sdb
# new > primary partition > set to 500 mb > beginning > bootable
# new > primary partition > set to ~90% > beginning
# new > primary partition > set to remaining space > beginning > type > 82 (linux swap)
# write


echo -e "export LFS=/mnt/lfs" >> /etc/profile
source /etc/profile

./3-partition.sh
./4-packages.sh
./5-prepare.sh

su - lfs
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF

cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH
EOF
```


```javascript
var a = 'asdf';
a.substr(0, 1);
```