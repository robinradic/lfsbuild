#!/bin/bash

mkfs -v -t ext4 /dev/sdb1
mkfs -v -t ext4 /dev/sdb2
mkswap /dev/sdb3
mkdir -pv $LFS/boot
mount -v -t ext4 /dev/sdb2 $LFS
mount -v -t ext4 /dev/sdb1 $LFS/boot
# /sbin/swapon -v /dev/<zzz>